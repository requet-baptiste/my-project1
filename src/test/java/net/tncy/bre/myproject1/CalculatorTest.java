package net.tncy.bre.myproject1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for Calculator.
 */
public class CalculatorTest {

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void shouldDivideCorrectly() {
        int n1, n2;
        n1 = 20;
        n2 = 5;
        try {
            assertEquals(Calculator.divide(n1, n2), n1 / n2);
        } catch (Exception e) {
            System.err.println();
        }
    }
}
