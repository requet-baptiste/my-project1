package net.tncy.bre.myproject1;

public class Calculator {

    public static int divide(int n1, int n2) throws Exception {
        if (n2 == 0) {
            throw new Exception("Cannot divide by 0");
        }
        return n1 / n2;
    }
}
